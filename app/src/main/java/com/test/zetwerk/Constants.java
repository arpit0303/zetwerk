package com.test.zetwerk;

public class Constants {

    public static final String STORAGE_URL = "gs://zetwerk-a5d1d.appspot.com/";

    public static final String STORAGE_NAME = "name";
    public static final String STORAGE_SALARY = "salary";
    public static final String STORAGE_SKILL = "skillSet";
    public static final String STORAGE_IMAGE_URI = "imageUri";
    public static final String STORAGE_DOB = "dob";
    public static final String STORAGE_ID = "id";

    public static final String SIZE = "size";
}
