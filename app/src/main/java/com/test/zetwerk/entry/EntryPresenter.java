package com.test.zetwerk.entry;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.test.zetwerk.Constants;
import com.test.zetwerk.R;
import com.test.zetwerk.utils.AppUtils;
import com.test.zetwerk.utils.Preferences;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class EntryPresenter implements EntryContract.Presenter {

    private EntryContract.View view;

    EntryPresenter(EntryContract.View view) {
        this.view = view;
    }

    @Override
    public void storeImage(final Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance(Constants.STORAGE_URL);

        StorageReference storageRef = storage.getReference();

        Uri file = getImageUri(bitmap);
        if(file != null) {
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("image/jpeg")
                    .build();

            final String lastPath = file.getLastPathSegment();
            String pathstring = "images/" + lastPath;

            UploadTask uploadTask = storageRef.child(pathstring).putFile(file, metadata);

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                }
            }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    view.uploadFailure(exception.getLocalizedMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    view.uploadSuccess(bitmap, lastPath);
                }
            });
        }
    }

    @Override
    public void storeData(String id, String name, String dob, String salary, String imageUri, String skillSet) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        final Map<String, String> user = new HashMap<>();
        user.put(Constants.STORAGE_ID, id);
        user.put(Constants.STORAGE_NAME, name);
        user.put(Constants.STORAGE_DOB, dob);
        user.put(Constants.STORAGE_SALARY, salary);
        user.put(Constants.STORAGE_IMAGE_URI, imageUri);
        user.put(Constants.STORAGE_SKILL, skillSet);

        String userId = Preferences.getUserId(view.getContext());

        db.collection(userId).document("" + id)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        view.storeSuccess(user);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        AppUtils.showToast(view.getContext(), e.getLocalizedMessage());
                        view.hideProgressBar();
                    }
                });
    }

    @Override
    public void getEmployee(String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String userId = Preferences.getUserId(view.getContext());
        DocumentReference document = db.collection(userId).document("" + id);
        document.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        getImageFromStorage(document);
                    } else {
                        view.hideProgressBar();
                    }
                } else {
                    view.hideProgressBar();
                }
            }
        });
    }

    private void getImageFromStorage(final DocumentSnapshot document) {
        FirebaseStorage storage = FirebaseStorage.getInstance(Constants.STORAGE_URL);

        StorageReference storageRef = storage.getReference();
        String pathString = "images/" + (String) document.get(Constants.STORAGE_IMAGE_URI);
        storageRef.child(pathString).getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                view.successfullyGetEmployee(document, bytes);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                view.hideProgressBar();
            }
        });
    }

    private Uri getImageUri(Bitmap inImage) {
        try {
            String path = MediaStore.Images.Media.insertImage(view.getContext().getContentResolver(), inImage, "Emp", null);
            return Uri.parse(path);
        }catch (NullPointerException e) {
            view.hideProgressBar();
            AppUtils.showToast(view.getContext(), view.getContext().getResources().getString(R.string.only_camera));
        }
        return null;
    }
}

