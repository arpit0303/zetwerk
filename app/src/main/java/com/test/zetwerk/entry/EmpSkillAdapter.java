package com.test.zetwerk.entry;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.test.zetwerk.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmpSkillAdapter extends RecyclerView.Adapter<EmpSkillAdapter.EmpSkillViewHolder> {

    private List<Boolean> skillList;

    EmpSkillAdapter(List<Boolean> skillList) {
        this.skillList = skillList;
    }

    @NonNull
    @Override
    public EmpSkillViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_skill, viewGroup, false);
        return new EmpSkillViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpSkillViewHolder empSkillViewHolder, int i) {
        CheckBox checkBox = empSkillViewHolder.cbEmpSkill;
        checkBox.setText("" + (i + 1));
        if (skillList.get(i))
            checkBox.setChecked(true);
        else
            checkBox.setChecked(false);

        final int position = i;

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                skillList.set(position, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return skillList.size();
    }

    class EmpSkillViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cb_emp_skill)
        CheckBox cbEmpSkill;

        EmpSkillViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
