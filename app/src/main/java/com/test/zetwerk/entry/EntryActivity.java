package com.test.zetwerk.entry;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.test.zetwerk.Constants;
import com.test.zetwerk.R;
import com.test.zetwerk.utils.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EntryActivity extends AppCompatActivity implements EntryContract.View {

    private EntryContract.Presenter presenter;

    private static final int SPAN_COUNT = 5;
    private static final int SKILL_SET = 10;
    private static final int REQUEST_CAMERA = 100;
    private static final int CAMERA_RESULT = 101;
    private static final int PICK_IMAGE = 102;

    @BindView(R.id.rv_emp_skill)
    RecyclerView rvEmpSkill;

    @BindView(R.id.til_emp_name)
    TextInputLayout tilEmpName;

    @BindView(R.id.tv_emp_dob)
    TextView tvEmpDOB;

    @BindView(R.id.til_emp_salary)
    TextInputLayout tilEmpSalary;

    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.iv_emp_photo)
    ImageView ivEmpPhoto;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.tv_emp_dob_hint)
    TextView tvEmpDobHint;

    String mUri = "";
    String id = "";
    Calendar myCalendar;
    List<Boolean> empSkillList = new ArrayList<>();
    EmpSkillAdapter adapter;
    boolean isNewEmployee = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        ButterKnife.bind(this);

        presenter = new EntryPresenter(this);

        progressBar.setVisibility(View.GONE);
        tvEmpDobHint.setVisibility(View.GONE);
        setUpEmpSkillList();

        if (getIntent() != null && getIntent().getStringExtra(Constants.STORAGE_ID) != null) {
            showProgressBar();
            isNewEmployee = false;
            presenter.getEmployee(getIntent().getStringExtra(Constants.STORAGE_ID));
        }


        if (getIntent() != null && getIntent().getStringExtra(Constants.SIZE) != null) {
            isNewEmployee = true;
            id = getIntent().getStringExtra(Constants.SIZE);
        }

        myCalendar = Calendar.getInstance();

        tvEmpDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EntryActivity.this, date, myCalendar
                        .get(Calendar.YEAR) - 20, myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            tvEmpDobHint.setVisibility(View.VISIBLE);
            tvEmpDOB.setText(sdf.format(myCalendar.getTime()));
        }
    };

    @OnClick(R.id.submit)
    public void submitClicked() {
        EditText etName = tilEmpName.getEditText();
        EditText etSalary = tilEmpSalary.getEditText();
        if (etName != null && etSalary != null) {
            String name = etName.getText().toString();
            String dob = tvEmpDOB.getText().toString();
            String salary = etSalary.getText().toString();
            String skill = "";

            for (int i = 0; i < empSkillList.size(); i++) {
                if (empSkillList.get(i)) {
                    int value = i + 1;
                    if (AppUtils.isNotEmpty(skill)) {
                        skill += "," + value;
                    } else {
                        skill += value;
                    }
                }
            }

            if (AppUtils.isNotEmpty(id) && AppUtils.isNotEmpty(name) && AppUtils.isNotEmpty(dob) && AppUtils.isNotEmpty(salary) && AppUtils.isNotEmpty(mUri) && AppUtils.isNotEmpty(skill)) {
                showProgressBar();
                presenter.storeData(id, name, dob, salary, mUri, skill);
            } else {
                AppUtils.showToast(EntryActivity.this, getResources().getString(R.string.fill_details));
            }
        } else {
            AppUtils.showToast(EntryActivity.this, getResources().getString(R.string.fill_details));
        }
    }

    @OnClick(R.id.tv_photo_delete)
    public void empPhotoDeleteClicked() {
        ivEmpPhoto.setImageResource(R.mipmap.ic_place_holder);
        mUri = "";
    }

    @OnClick(R.id.tv_photo_add)
    public void empPhotoAddClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAMERA);
            } else {
                openCamera();
            }
        } else {
            openCamera();
        }

    }

    private void openCamera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EntryActivity.this);

        String[] options = {"Camera", "Gallery", "Cancel"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        startActivityForResult(intent, CAMERA_RESULT);
                        break;
                    case 1:
                        Intent imageIntent = new Intent();
                        imageIntent.setType("image/jpeg");
                        imageIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(imageIntent, "Select Picture"), PICK_IMAGE);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED
                    || grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                if (showRationale) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EntryActivity.this)
                            .setTitle(getResources().getString(R.string.mandatory))
                            .setMessage(getResources().getString(R.string.msg))
                            .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            } else {
                openCamera();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_RESULT:
                    if (data != null && data.getExtras() != null) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        showProgressBar();
                        presenter.storeImage(photo);
                    }
                    break;
                case PICK_IMAGE:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    showProgressBar();
                    presenter.storeImage(bitmap);
                    break;
            }
        }
    }

    private void setUpEmpSkillList() {
        for (int i = 0; i < SKILL_SET; i++) {
            empSkillList.add(false);
        }
        adapter = new EmpSkillAdapter(empSkillList);
        rvEmpSkill.setLayoutManager(new GridLayoutManager(EntryActivity.this, SPAN_COUNT));
        rvEmpSkill.setAdapter(adapter);
    }

    @Override
    public Context getContext() {
        return EntryActivity.this;
    }

    @Override
    public void uploadFailure(String errorMsg) {
        hideProgressBar();
        ivEmpPhoto.setImageResource(R.mipmap.ic_place_holder);
        AppUtils.showToast(EntryActivity.this, errorMsg);
    }

    @Override
    public void uploadSuccess(Bitmap bitmap, String uri) {
        hideProgressBar();
        ivEmpPhoto.setImageBitmap(bitmap);
        mUri = uri;
    }

    @Override
    public void storeSuccess(Map<String, String> user) {
        hideProgressBar();

        if (isNewEmployee) {
            AppUtils.showToast(EntryActivity.this, getResources().getString(R.string.success));
            Intent resultIntent = new Intent();
            resultIntent.putExtra(Constants.STORAGE_ID, user.get(Constants.STORAGE_ID));
            resultIntent.putExtra(Constants.STORAGE_DOB, user.get(Constants.STORAGE_DOB));
            resultIntent.putExtra(Constants.STORAGE_IMAGE_URI, user.get(Constants.STORAGE_IMAGE_URI));
            resultIntent.putExtra(Constants.STORAGE_NAME, user.get(Constants.STORAGE_NAME));
            resultIntent.putExtra(Constants.STORAGE_SALARY, user.get(Constants.STORAGE_SALARY));
            resultIntent.putExtra(Constants.STORAGE_SKILL, user.get(Constants.STORAGE_SKILL));
            setResult(Activity.RESULT_OK, resultIntent);
        } else {
            AppUtils.showToast(EntryActivity.this, getResources().getString(R.string.updated));
            setResult(Activity.RESULT_OK, null);
        }
        finish();
    }

    @Override
    public void successfullyGetEmployee(DocumentSnapshot documentSnapshot, byte[] bytes) {
        hideProgressBar();
        tilEmpName.getEditText().setText((String) documentSnapshot.get(Constants.STORAGE_NAME));
        tvEmpDobHint.setVisibility(View.VISIBLE);
        tvEmpDOB.setText((String) documentSnapshot.get(Constants.STORAGE_DOB));
        tilEmpSalary.getEditText().setText((String) documentSnapshot.get(Constants.STORAGE_SALARY));
        mUri = (String) documentSnapshot.get(Constants.STORAGE_IMAGE_URI);
        String position = (String) documentSnapshot.get(Constants.STORAGE_SKILL);
        id = (String) documentSnapshot.get(Constants.STORAGE_ID);

        if (AppUtils.isNotEmpty(position)) {
            String[] skillSet = position.split(",");
            for (String skill : skillSet) {
                int index = Integer.parseInt(skill) - 1;
                empSkillList.set(index, true);
                adapter.notifyItemChanged(index);
            }

        }

        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        ivEmpPhoto.setImageBitmap(bmp);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
