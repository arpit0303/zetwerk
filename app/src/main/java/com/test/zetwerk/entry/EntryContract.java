package com.test.zetwerk.entry;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.Map;

public interface EntryContract {

    interface View {

        Context getContext();

        void uploadFailure(String errorMsg);

        void uploadSuccess(Bitmap bitmap, String uri);

        void storeSuccess(Map<String, String> user);

        void successfullyGetEmployee(DocumentSnapshot documentSnapshot, byte[] bytes);

        void hideProgressBar();

    }

    interface Presenter{
        void storeImage(Bitmap bitmap);

        void storeData(String id, String name, String dob, String salary, String imageUri, String skillSet);

        void getEmployee(String id);
    }
}
