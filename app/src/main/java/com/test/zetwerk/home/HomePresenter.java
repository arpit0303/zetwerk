package com.test.zetwerk.home;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.test.zetwerk.Constants;
import com.test.zetwerk.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;

    HomePresenter(HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void getEmployees() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String userId = Preferences.getUserId(view.getContext());
        CollectionReference collection = db.collection(userId);
        collection.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    QuerySnapshot document = task.getResult();
                    if (document != null && document.size() > 0) {
                        setBean(document.getDocuments());
                    } else {
                        view.hideProgressBar();
                    }
                } else {
                    view.hideProgressBar();
                }
            }
        });
    }

    private void setBean(List<DocumentSnapshot> documentSnapshotList) {
        List<EmployeeBean> employeeBeanList = new ArrayList<>();
        for(DocumentSnapshot documentSnapshot: documentSnapshotList) {
            EmployeeBean bean = new EmployeeBean();
            bean.setId((String) documentSnapshot.get(Constants.STORAGE_ID));
            bean.setDob((String) documentSnapshot.get(Constants.STORAGE_DOB));
            bean.setImageUri((String) documentSnapshot.get(Constants.STORAGE_IMAGE_URI));
            bean.setName((String) documentSnapshot.get(Constants.STORAGE_NAME));
            bean.setSalary((String) documentSnapshot.get(Constants.STORAGE_SALARY));
            bean.setSkill((String) documentSnapshot.get(Constants.STORAGE_SKILL));

            employeeBeanList.add(bean);
        }
        view.successfullyGetEmployeeList(employeeBeanList);
    }
}
