package com.test.zetwerk.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.test.zetwerk.Constants;
import com.test.zetwerk.R;
import com.test.zetwerk.entry.EntryActivity;
import com.test.zetwerk.main.MainActivity;
import com.test.zetwerk.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeContract.View {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rvEmployee)
    RecyclerView rvEmployee;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    int empSize = 0;

    private HomeContract.Presenter presenter;
    EmployeeAdapter adapter;
    List<EmployeeBean> employeeBeanList;

    public static final int RESULT_ENTRY = 201;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        if (!Preferences.isLogin(HomeActivity.this)) {
            exit();
        } else {
            setSupportActionBar(toolbar);
            employeeBeanList = new ArrayList<>();
            adapter = new EmployeeAdapter(employeeBeanList, HomeActivity.this);
            rvEmployee.setLayoutManager(new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false));
            rvEmployee.setAdapter(adapter);

            presenter = new HomePresenter(this);
            showProgressBar();
            presenter.getEmployees();
        }
    }

    @OnClick(R.id.exit)
    public void exit() {
        Preferences.clearSharedPreferences(HomeActivity.this);
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.fab)
    public void fabClicked() {
        Intent intent = new Intent(HomeActivity.this, EntryActivity.class);
        intent.putExtra(Constants.SIZE, empSize + 1 + "");
        startActivityForResult(intent, RESULT_ENTRY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RESULT_ENTRY:
                    if (data != null) {
                        EmployeeBean bean = new EmployeeBean();
                        bean.setId(data.getStringExtra(Constants.STORAGE_ID));
                        bean.setDob(data.getStringExtra(Constants.STORAGE_DOB));
                        bean.setSalary(data.getStringExtra(Constants.STORAGE_SALARY));
                        bean.setSkill(data.getStringExtra(Constants.STORAGE_SKILL));
                        bean.setName(data.getStringExtra(Constants.STORAGE_NAME));
                        bean.setImageUri(data.getStringExtra(Constants.STORAGE_IMAGE_URI));
                        employeeBeanList.add(bean);
                        adapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
    }

    @Override
    public Context getContext() {
        return HomeActivity.this;
    }

    @Override
    public void successfullyGetEmployeeList(List<EmployeeBean> beanList) {
        hideProgressBar();
        this.employeeBeanList = beanList;
        adapter = new EmployeeAdapter(employeeBeanList, HomeActivity.this);
        rvEmployee.setLayoutManager(new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false));
        rvEmployee.setAdapter(adapter);
        empSize = employeeBeanList.size();
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
