package com.test.zetwerk.home;

import android.content.Context;

import java.util.List;

public interface HomeContract {

    interface View {
        Context getContext();

        void successfullyGetEmployeeList(List<EmployeeBean> employeeBeanList);

        void hideProgressBar();
    }

    interface Presenter {

        void getEmployees();
    }
}
