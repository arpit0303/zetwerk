package com.test.zetwerk.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.zetwerk.Constants;
import com.test.zetwerk.R;
import com.test.zetwerk.entry.EntryActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    private List<EmployeeBean> employeeBeanList;
    private Context context;
    private Activity activity;

    EmployeeAdapter(List<EmployeeBean> employeeBeanList, Activity activity) {
        this.employeeBeanList = employeeBeanList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_employee, viewGroup, false);
        return new EmployeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder employeeViewHolder, int i) {
        EmployeeBean employeeBean = employeeBeanList.get(i);
        employeeViewHolder.tvEmpId.setText(employeeBean.getId());
        employeeViewHolder.tvEmpName.setText(employeeBean.getName());

        final String id = employeeBean.getId();

        employeeViewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EntryActivity.class);
                intent.putExtra(Constants.STORAGE_ID, id);
                activity.startActivityForResult(intent, HomeActivity.RESULT_ENTRY);
            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeBeanList.size();
    }

    class EmployeeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_employee_name)
        TextView tvEmpName;

        @BindView(R.id.tv_employee_id)
        TextView tvEmpId;

        @BindView(R.id.cvItem)
        CardView cvItem;

        EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
