package com.test.zetwerk.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.test.zetwerk.R;
import com.test.zetwerk.home.HomeActivity;
import com.test.zetwerk.utils.AppUtils;
import com.test.zetwerk.utils.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainContract.Presenter presenter;

    @BindView(R.id.til_mobile)
    TextInputLayout tilMobile;

    @BindView(R.id.til_otp)
    TextInputLayout tilOtp;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.btn_verify)
    Button btnVerify;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tilMobile.setVisibility(View.VISIBLE);
        btnLogin.setVisibility(View.VISIBLE);
        tilOtp.setVisibility(View.GONE);
        btnVerify.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        presenter = new MainPresenter(this);
    }

    @OnClick(R.id.btn_login)
    public void loginClicked() {
        EditText editText = tilMobile.getEditText();
        if (editText != null) {
            String mobile = editText.getText().toString();
            if (AppUtils.validateMobile(mobile)) {
                showProgressBar();
                presenter.sendOtp(mobile);
            }
        }
    }

    @OnClick(R.id.btn_verify)
    public void verifyClicked() {
        EditText editText = tilOtp.getEditText();
        if (editText != null) {
            String otp = editText.getText().toString();
            if (AppUtils.validateOtp(otp)) {
                showProgressBar();
                presenter.verifyOtp(otp);
            }
        }
    }

    @Override
    public Activity getActivity() {
        return MainActivity.this;
    }

    @Override
    public void successfullySendOtp() {
        hideProgressBar();
        tilMobile.setVisibility(View.GONE);
        btnLogin.setVisibility(View.GONE);
        tilOtp.setVisibility(View.VISIBLE);
        btnVerify.setVisibility(View.VISIBLE);
    }

    @Override
    public void successfullyLogin() {
        hideProgressBar();
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Preferences.setLoginDone(MainActivity.this);
        startActivity(intent);
        finish();
    }

    private void showProgressBar() {
        AppUtils.hideKeyboard(MainActivity.this);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
