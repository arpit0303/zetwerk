package com.test.zetwerk.main;

import android.app.Activity;

public interface MainContract {

    interface View {

        Activity getActivity();

        void successfullySendOtp();

        void successfullyLogin();

        void hideProgressBar();
    }

    interface Presenter {
        void sendOtp(String mobile);

        void verifyOtp(String otp);
    }
}
