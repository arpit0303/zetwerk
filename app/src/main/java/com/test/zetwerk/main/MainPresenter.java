package com.test.zetwerk.main;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.test.zetwerk.R;
import com.test.zetwerk.utils.AppUtils;
import com.test.zetwerk.utils.Preferences;

import java.util.concurrent.TimeUnit;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private String mVerificationId;

    MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void sendOtp(String mobile) {
        mobile = "+91-" + mobile;

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile, 60, TimeUnit.SECONDS,
                view.getActivity(),
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        AppUtils.showToast(view.getActivity(), view.getActivity().getResources().getString(R.string.error));
                        view.hideProgressBar();
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                        mVerificationId = verificationId;
                        view.successfullySendOtp();
                    }
                });
    }

    @Override
    public void verifyOtp(String otp) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(view.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            FirebaseUser user = task.getResult().getUser();
                            Preferences.setUserId(view.getActivity(), user.getPhoneNumber());
                            view.successfullyLogin();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                AppUtils.showToast(view.getActivity(), view.getActivity().getResources().getString(R.string.invalid_otp));
                                view.hideProgressBar();
                            }
                        }
                    }
                });
    }
}
