package com.test.zetwerk.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


public class AppUtils {

    public static Boolean isNotEmpty(String str) {
        return str != null && !str.trim().isEmpty();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean validateMobile(String mobile) {
        return isNotEmpty(mobile) && mobile.length() == 10 && (mobile.startsWith("6") || mobile.startsWith("7") || mobile.startsWith("8") || mobile.startsWith("9"));
    }

    public static boolean validateOtp(String otp) {
        return isNotEmpty(otp) && otp.length() == 6;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
